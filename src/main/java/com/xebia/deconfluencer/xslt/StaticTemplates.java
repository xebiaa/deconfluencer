/**
 * Copyright (c) 2011, Wilfred Springer
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the <organization>.
 * 4. Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.xebia.deconfluencer.xslt;

import com.xebia.deconfluencer.log.Logger;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

/**
 * A {@link javax.xml.transform.Templates} object that will not return the configured XSL file without checking for
 * modifications on the file system.
 */
public class StaticTemplates implements Templates, Observer {

    private final Logger logger = Logger.forClass(StaticTemplates.class);

    /**
     * The file to be monitored.
     */
    private final File file;

    /**
     * The factory used for constructing XSLT.
     */
    private final TransformerFactory factory;

    private volatile Templates templates;

    public StaticTemplates(File file, TransformerFactory factory) {
        this.file = file;
        this.factory = factory;
        this.templates = load(file);
        try {
            // Not supposed to leak this-references before the constructor completes, yeah, yeah...
            ReloadTrigger.install(this);
            logger.info("Send SIGHUP to reload the XSLT template.");
        } catch (Throwable e) {
            logger.info("Restart deconfluencer to reload the XSLT template");
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        // This is not threadsafe. We'll accept that until it proves problematic.
        this.templates = load(file);
    }

    @Override
    public Transformer newTransformer() throws TransformerConfigurationException {
        return templates.newTransformer();
    }

    @Override
    public Properties getOutputProperties() {
        return templates.getOutputProperties();
    }

    private Templates load(File file) {
        logger.info("Loading XSLT from file " + file);
        try {
            Source source = new StreamSource(file);
            return factory.newTemplates(source);
        } catch (TransformerConfigurationException e) {
            logger.error("Failed to load XSLT file.", e);
            return new NullTemplates();
        }
    }

    /**
     * Installs a signal handler to respond to the SIGHUP signal. Implemented as a static inner class so as not to
     * prevent the enclosing class from loading in JVM's where sun.misc.SignalHandler is unavailable. Do wrap your call
     * to {@code install()} in a try/catch block, because you would get a {@code NoClassDefFoundError}.
     */
    private static class ReloadTrigger extends Observable implements SignalHandler {
        static void install(Observer observer) {
            Signal.handle(new Signal("HUP"), new ReloadTrigger(observer));
        }

        private ReloadTrigger(Observer observer) {
            super();
            this.addObserver(observer);
        }

        @Override
        public void handle(Signal signal) {
            if ("HUP".equals(signal.getName())) {
                this.setChanged();
                this.notifyObservers();
            }
        }
    }
}
